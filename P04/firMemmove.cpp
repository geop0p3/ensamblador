#include <iostream>
#include <string.h>
#include <time.h>
#include <chrono>

//Filtro FIR con memmove
using namespace std;

class FIR{
    double *coef;
    double *samples;
    int tam;

public:
    FIR(int n);
    ~FIR();

    void iniciaCoef(double C[], int n);
    double directa( double s);
    double directaMM( double s);

};

FIR::FIR(int n)
{
    tam = n;
    coef = new double[tam];
    samples = new double[tam];

}

FIR::~FIR()
{
    delete coef;
    delete samples;
}

void FIR::iniciaCoef(double C[], int n)
{
    if( tam == n)
    {
        for (int i =0; i<tam; i++)
        {
            coef[i] = C[i];
            samples[i] = 0.0;
        }
    }
    else
    {
        delete coef;
        delete samples;
        tam = n;
        coef = new double[tam];
        samples = new double[tam];
        for (int i =0; i<tam; i++)
        {
            coef[i] = C[i];
            samples[i] = 0.0;
        }

    }


}

double FIR::directa(double s)
{
    double out = 0.0;
    int i;
    samples[0] =s;

    for (i=0; i<tam; i++)
    {

        out += samples[i]*coef[i];
    }
    for (i = tam-1; i>0 ; i--)
    {

        samples[i] = samples[i-1];
    }

    return out;
}

double FIR::directaMM(double s)
{
    double out = 0.0;
    int i;
    samples[0] =s;


    for (i=0; i<tam; i++)
    {

        out += samples[i]*coef[i];
    }
    memmove(samples +1, samples, (tam-1)* sizeof(double));
    return out;
}


int main()
{
    srand(time(NULL));
    FIR unfiltro(160);
    double coeficientes[] = {0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25,
    0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25};
    unfiltro.iniciaCoef( coeficientes, 160);
    int ciclos = 320000;

    for (int j=0; j<15; j++)
    {

        //Medir tiempo
        cout<<"Filtro sin memmocopy:  ";
        auto start = std::chrono::system_clock::now();
        for (int i = 0; i<ciclos; i++ )
        {
            unfiltro.directa(rand());
        }
        auto fin = std::chrono::system_clock::now();

        std::chrono::duration<double> elapsed = fin - start;
        std::cout << "Elapsed time: " << elapsed.count() << "s"<<endl;


        cout<<"Filtro CON memmocopy:  ";
        start = std::chrono::system_clock::now();
        for (int i = 0; i<ciclos; i++ )
        {
            unfiltro.directa(rand());
        }
        fin = std::chrono::system_clock::now();

        elapsed = fin - start;
        std::cout << "Elapsed time: " << elapsed.count() << "s";
        cout<<endl<<"__________________________"<<endl;
        }


    return 0;

}

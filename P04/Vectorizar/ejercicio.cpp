#include "workshop.h"

int mag = 30;
int tamanio = 3;
double coeficientes[] = {0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25,
    0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.25,0.25,0.25, 0.5, 0.5, 0.5, 0.25, 0.25, 0.25};

int main(int argc, char **argv)
{
    auto timer = workshop::start_timer();

    for (int i=0; i<tamanio; i++) {
        coeficientes[i] = coeficientes[i] / mag;
    }

    auto duration = workshop::get_duration(timer);

    timer = workshop::start_timer();

    #pragma omp simd
    for (int i=0; i<tamanio; i++) {
        coeficientes[i] = coeficientes[i] / mag;
    }

    auto vector_duration = workshop::get_duration(timer);

    std::cout << "Loop sin vector " << duration
              << " microsegundos para completar." << std::endl;

    std::cout << "Loop con vector " << vector_duration
              << " microsegundos para completar." << std::endl;

    return 0;
}

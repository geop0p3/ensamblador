; move
; Moves a sprite on the screen and flips it when it changes direction
; FIXED sprites- R
.INCLUDE "../lib/header.asm"
.INCLUDE "../lib/registers.asm"
.INCLUDE "../lib/settings.asm"
.INCLUDE "../lib/values.asm"
.INCLUDE "../lib/initialization.asm"

.BANK 0
.ORG 0

.SECTION ""

Main:
    Reset

    ; Set accumulator to 8-bit mode
    sep #$20

    ; Create a color for the sprite tile
    lda #$81
    sta CGADD
    lda #$FF
    sta CGDATA
    lda #$FF
    sta CGDATA

    ; Set sprite size small to 8 by 8 pixels, large to 16 by 16 pixels
    ; and sprite character segment to 0
    stz OBSEL
    ; Create variables for the sprite coordinates
    .DEFINE SPRITE_X $7E0000
    .DEFINE SPRITE_Y $7E0001
    .DEFINE SPRITE_X_2 $7E0002
    .DEFINE SPRITE_Y_2 $7E0003
    .DEFINE SPRITE_X_3 $7E0005
    .DEFINE SPRITE_Y_3 $7E0006
	;.DEFINE RANDOMVAR $7E000A
	
	.DEFINE estaUpRi $7E000A     ;(indica direccion) b 0000 0011 - 0000 0010
	lda #$03
	sta estaUpRi
    ; Load variables
    lda #(SCREEN_W / 2 - 120)
    sta SPRITE_X
    lda #(SCREEN_H / 2 - 4)
    sta SPRITE_Y

    lda #(SCREEN_W / 2 + 100)
    sta SPRITE_X_2
    lda #(SCREEN_H / 2 - 4)
    sta SPRITE_Y_2

    lda #(SCREEN_W / 2 - 4)
    sta SPRITE_X_3
    lda #(SCREEN_H / 2 - 4)
    sta SPRITE_Y_3


    ; Select sprite table 1 record 0
    stz OAMADDH
    stz OAMADDL

    ; Create record objeto 0 (jugador 1)
    lda SPRITE_X
    sta OAMDATA
    lda SPRITE_Y
    sta OAMDATA
    lda #$02
    sta OAMDATA
    stz OAMDATA


    lda SPRITE_X_2 ;objeto 1 (jugador 2)
    sta OAMDATA
    lda SPRITE_Y_2
    sta OAMDATA
    lda #$03
    sta OAMDATA
    stz OAMDATA

    lda SPRITE_X_3 ;objeto 2 (pelota)
    sta OAMDATA
    lda SPRITE_Y_3
    sta OAMDATA
    lda #$04
    sta OAMDATA
    stz OAMDATA

    ; Select sprite table 2 record 0-3
    ;lda #$01
    ;sta OAMADDH
    ;stz OAMADDL

    ; Create records
    stz OAMDATA

    ; Set VRAM write mode to increment after every writing to VMDATAL
    lda #VMAINC_INC_L
    sta VMAINC

    ; Create character 1 in sprite character segment 0 (@4bpp)
    lda #$10
    sta VMADD

    ; Arrow up/down
    lda #%00011100
    sta VMDATAL
    lda #%00011100
	sta	VMDATAL
    lda #%00011100
    sta VMDATAL
    lda #%00011100
    sta VMDATAL
    lda #%00011100
    sta VMDATAL
    lda #%00011100
    sta VMDATAL
    lda #%00011100
    sta VMDATAL
    lda #%00011100
    sta VMDATAL


    lda #$20
    sta VMADD

    ; Arrow up/down
    lda #%00011100
    sta VMDATAL
    lda #%00011100
    sta VMDATAL
    lda #%00011100
    sta VMDATAL
    lda #%00011100
    sta VMDATAL
    lda #%00011100
    sta VMDATAL
    lda #%00011100
    sta VMDATAL
    lda #%00011100
    sta VMDATAL
    lda #%00011100
    sta VMDATAL


    lda #$03
    sta OAMADDH
    stz OAMADDL

    ; Create records
    stz OAMDATA

    ; Set VRAM write mode to increment after every writing to VMDATAL
    lda #VMAINC_INC_L
    sta VMAINC
     ; Segundo Jugador
    lda #$30
    sta VMADD

    lda #%00011100
    sta VMDATAL
    lda #%00011100
    sta VMDATAL
    lda #%00011100
    sta VMDATAL
    lda #%00011100
    sta VMDATAL
    lda #%00011100
    sta VMDATAL
    lda #%00011100
    sta VMDATAL
    lda #%00011100
    sta VMDATAL
    lda #%00011100
    sta VMDATAL


    lda #$40
    sta VMADD

    lda #%00000000
    sta VMDATAL
    lda #%00000000
    sta VMDATAL
    lda #%00000000
    sta VMDATAL
    lda #%00011000
    sta VMDATAL
    lda #%00011000
    sta VMDATAL
    lda #%00000000
    sta VMDATAL
    lda #%00000000
    sta VMDATAL
    lda #%00000000
    sta VMDATAL


    ; Enable the sprite layer
    lda #%00010000
    sta TM

    ; Enable screen and set it to full brightness
    lda #$0F
    sta INIDISP

    ; Enable VBlank and joypad auto-read
    lda #(NMITIMEN_NMI_ENABLE | NMITIMEN_JOY_ENABLE)
    sta NMITIMEN

-   wai
	
	sep #$20 ;cambiar A a modo 8 bit
	lda estaUpRi 
	bit #$01		;moviendose a la derecha? bit 0 de estaUpRi=1
	bne ballRight ;if(mueveDerecha) {X++}
	jmp ballLeft
sigueR:	
	lda estaUpRi    
	bit #$02
	bne tballUp
	jmp tballDo

sigueLL:
	lda SPRITE_X_3  ;0111 1111
	CMP #(SCREEN_W - 10) ;if(pelotaX < width -10) {cambiar a x--}
    BCC noCambiarHa ;empezar a mover a la izquierda
	lda estaUpRi ;xxxx xxxH
	and #$FE     ;1111 1110 = xxxx xxx0
	;lda #$00 ;0000 00000
	sta estaUpRi  ;indicar 
noCambiarHa:

	lda SPRITE_X_3 ;if (pelotaX > 10) {cambiar a x++}
	CMP #$0A
	BCS noCambiarHb
	lda estaUpRi   ;xxxx xxxH
	ora #$01       ;0000 0001 = xxxx xxx1
    ; Select sprite table 1
	sta estaUpRi
	;CMP #$07 ;si la coord X de pelot < 7 empezar a mover a la derecha
	;BCC ballRight
noCambiarHb:	
sigue:
	nop
    jmp -


ballLeft:
	sep #$20
	lda estaUpRi ;xxxx xxxH
	and #$FE     ;1111 1110 = xxxx xxx0
	;lda #$00 ;0000 00000
	sta estaUpRi  ;indicar que se esta moviendo a la izquierda
    ; Select sprite table 1
    lda #$00
    sta OAMADDH
	lda #$04
    sta OAMADDL
    lda SPRITE_X_3
    dec a
    sta SPRITE_X_3
    sta OAMDATA
	lda SPRITE_Y_3
	sta OAMDATA
    lda #$04
    sta OAMDATA
    stz OAMDATA
	;lda RANDOMVAR
	;inc a
	;sta RANDOMVAR
	jmp sigueR

tballUp:
	jmp ballUp
tballDo:
	jmp ballDown


ballRight:
	sep #$20
	lda estaUpRi   ;xxxx xxxH
	ora #$01       ;0000 0001 = xxxx xxx1
    ; Select sprite table 1
	sta estaUpRi
    lda #$00
    sta OAMADDH
	lda #$04
    sta OAMADDL
    lda SPRITE_X_3
    inc a
    sta SPRITE_X_3
    sta OAMDATA
	lda SPRITE_Y_3
	sta OAMDATA
    lda #$04
    sta OAMDATA
    stz OAMDATA
	jmp sigueR

ballUp:
	sep #$20
	lda estaUpRi ;xxxx xxVx
	ora #$02     ;0000 0010
	sta estaUpRi  ;indicar que se esta moviendo a la izquierda
    ; Select sprite table 1
    lda #$00
    sta OAMADDH
	lda #$04
    sta OAMADDL
    lda SPRITE_X_3
    sta OAMDATA
	lda SPRITE_Y_3
	dec a
	sta SPRITE_Y_3
	sta OAMDATA
    lda #$04
    sta OAMDATA
    stz OAMDATA
	;lda RANDOMVAR
	;inc a
	;sta RANDOMVAR
	jmp sigueLL

ballDown:
	sep #$20
	lda estaUpRi ;xxxx xxVx
	and #$fb     ;1111 1101
	sta estaUpRi  ;indicar
    ; Select sprite table 1
    lda #$00
    sta OAMADDH
	lda #$04
    sta OAMADDL
    sta SPRITE_X_3
    sta OAMDATA
	lda SPRITE_Y_3
	inc a
	sta SPRITE_Y_3
	sta OAMDATA
    lda #$04
    sta OAMDATA
    stz OAMDATA
	jmp sigueLL


VBlank:
    ; Wait for joypad auto-read
-   lda HVBJOY
    bit #HVBJOY_JOYREADY
    bne -

    ; Reset accumulator to 16-bit
    rep #$20

    ; Get joypad input
    lda JOY1

    ; Check if up was pressed on the D-pad
    bit #JOY_UP
    bne Up

    ; Check if down was pressed on the D-pad
    bit #JOY_DOWN
    bne Down

    lda JOY2

    ; Check if up was pressed on the D-pad
    bit #JOY_UP
    bne Up2

    ; Check if down was pressed on the D-pad
    bit #JOY_DOWN
    bne Down2

    rti
;jugador 1 arriba
Up:
    ; Set accumulator to 8-bit mode
    sep #$20

    ; Select sprite table 1
    lda #$00
    sta OAMADDH

    ; Enable sprite 0 vertical flip and move it up
    stz OAMADDL
    lda SPRITE_X
    sta OAMDATA
    lda SPRITE_Y
    dec a
    dec a
    sta SPRITE_Y
    sta OAMDATA
    lda #$01
    sta OAMDATA
    ;lda #%10000000
    stz OAMDATA

    rti

Up2:
    ; Set accumulator to 8-bit mode
    sep #$20

    ; Select sprite table 1
    lda #$02
    sta OAMADDH

    ; Enable sprite 0 vertical flip and move it up
	lda #$02
    sta OAMADDL
    lda SPRITE_X_2
    sta OAMDATA
    lda SPRITE_Y_2
    dec a
    dec a
    sta SPRITE_Y_2
    sta OAMDATA
    lda #$01
    sta OAMDATA
    lda #%10000000
    sta OAMDATA

    rti

Down:
    ; Set accumulator to 8-bit mode
    sep #$20

    ; Select sprite table 1
    lda #$00
    sta OAMADDH

    ; Disable sprite 0 vertical flip
    stz OAMADDL
    lda SPRITE_X
    sta OAMDATA
    lda SPRITE_Y
    inc a
    inc a
    sta SPRITE_Y
    sta OAMDATA
    lda #$01
    sta OAMDATA
    stz OAMDATA

    rti

Down2:
    ; Set accumulator to 8-bit mode
    sep #$20

    ; Select sprite table 1
    lda #$02
    sta OAMADDH

    ; Disable sprite 0 vertical flip
	lda #$02
    sta OAMADDL
    lda SPRITE_X_2
    sta OAMDATA
    lda SPRITE_Y_2
    inc a
    inc a
    sta SPRITE_Y_2
    sta OAMDATA
    lda #$01
    sta OAMDATA
    stz OAMDATA

    rti

IRQ:
    lda TIMEUP
    rti

.ENDS









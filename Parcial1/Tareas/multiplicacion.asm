;                     _              _                                     
;                    | |    o       | | o                  o               
;   _  _  _          | |_|_      _  | |     __   __,   __      __   _  _   
;  / |/ |/ |  |   |  |/  |  |  |/ \_|/  |  /    /  |  /    |  /  \_/ |/ |  
;    |  |  |_/ \_/|_/|__/|_/|_/|__/ |__/|_/\___/\_/|_/\___/|_/\__/   |  |_/
;                             /|                                           
;                             \|                                           
;  


LDX #04
STX $25
LDA #03
STA $34 
LDY #00
LDA #00
TIMES:
    ADC $25
    INY
    JMP CHECK
CHECK:
    CPY $34
    BNE TIMES
    JMP END

END:
    BRK

;    ___                      _                  _   _       
;   / _ \                    | |                | \ | |      
;  / /_\ \_ __ _ __ ___  __ _| | ___    ______  |  \| | ___  
;  |  _  | '__| '__/ _ \/ _` | |/ _ \  |______| | . ` |/ _ \ 
;  | | | | |  | | |  __/ (_| | | (_) |          | |\  | (_) |
;  \_| |_/_|  |_|  \___|\__, |_|\___/           \_| \_/\___/ 
;                        __/ |                               
;                       |___/                                
;   _____ _                
;  /  ___(_)               
;  \ `--. _ _ ____   _____ 
;   `--. \ | '__\ \ / / _ \
;  /\__/ / | |   \ V /  __/
;  \____/|_|_|    \_/ \___|
;                          
;                          
;  

start:
    LDX #00
    LDY #01
compare:
    LDA valores, X
    STA $01
    INX
    LDY valores, X
    STY $02
    DEX
    CMP $02
    BCC store
    TYA
    STA ordenado, X
    INX
    JMP compare

store:
    STA ordenado, X
    INX
    JMP compare

incX:
    INX
    JMP compare

valores:
    dcb 3,2,1

ordenado:
    dcb 1,0,0
